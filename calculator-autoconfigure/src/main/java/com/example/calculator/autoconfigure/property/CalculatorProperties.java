package com.example.calculator.autoconfigure.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "calculator")
@Getter
@Setter
public class CalculatorProperties {
	private boolean useAutoConfigure;
	private String resultPrefix;
}
