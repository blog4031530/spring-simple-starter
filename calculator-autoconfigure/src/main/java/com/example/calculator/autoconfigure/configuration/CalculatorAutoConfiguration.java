package com.example.calculator.autoconfigure.configuration;

import com.example.calculator.Calculator;
import com.example.calculator.autoconfigure.property.CalculatorProperties;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
@ConditionalOnClass({Calculator.class})
@EnableConfigurationProperties(CalculatorProperties.class)
@ConditionalOnProperty(prefix = "calculator", name = "use-auto-configure", havingValue = "true")
public class CalculatorAutoConfiguration {

	@ConditionalOnMissingBean(Calculator.class)
	@Bean
	public Calculator calculator(CalculatorProperties calculatorProperties) {
		return new Calculator(calculatorProperties.getResultPrefix());
	}
}
