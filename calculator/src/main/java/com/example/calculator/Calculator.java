package com.example.calculator;

import java.util.function.BiFunction;

public class Calculator {

	private final String resultPrefix;
	public Calculator(String resultPrefix) {
		this.resultPrefix = resultPrefix;
	}

	public String calculate(int x,
							int y,
							BiFunction<Integer, Integer, Integer> what) {
		return what.andThen((result) -> resultPrefix + " " + result).apply(x, y);
	}
}
