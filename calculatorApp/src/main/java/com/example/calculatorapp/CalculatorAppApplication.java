package com.example.calculatorapp;

import com.example.calculator.Calculator;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class CalculatorAppApplication implements CommandLineRunner {

	private final Calculator calculator;

	public static void main(String[] args) {
		SpringApplication.run(CalculatorAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String result = calculator.calculate(1, 2, Integer::sum);
		System.out.println(result);

		result = calculator.calculate(2, 3, (x, y) -> x * y);
		System.out.println(result);

		result = calculator.calculate(4, 5, (x, y) -> x * y);
		System.out.println(result);
	}
}
